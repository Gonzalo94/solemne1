/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

/**
 *
 * @author Gonzalo
 */
public class Calculadora {

    /**
     * @return the C
     */
    public int getC() {
        return C;
    }

    /**
     * @param C the C to set
     */
    public void setC(int C) {
        this.C = C;
    }

    /**
     * @return the i
     */
    public int geti() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void seti(int i) {
        this.i = i;
    }

    /**
     * @return the n
     */
    public int getN() {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(int n) {
        this.n = n;
    }

    /**
     * @return the res
     */
    public int getRes() {
        return  this.C * this.i/100 * this.n;
    }

    /**
     * @param res the res to set
     */
    public void setRes(int res) {
        this.res = res;
    }
    
    private int C;// Capital
    private int i;//tasa interés anual
    private int n;// numero de años
    private int res;// resultado de operacion
    
}
